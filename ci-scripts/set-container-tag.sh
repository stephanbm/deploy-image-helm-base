#!/usr/bin/env bash

# Determines the DEPLOYER_TAG

set_tag() {
    DEPLOYER_TAG="$1"
    echo "Setting DEPLOYER_TAG to ${DEPLOYER_TAG}"
    export DEPLOYER_TAG
}

# Prefer the tag, don't leave it to chance
if [ -n "${CI_COMMIT_TAG}" ]; then
    set_tag "${CI_COMMIT_TAG}"
elif [ -n "${CI_COMMIT_REF_NAME}" ]; then
    set_tag "${CI_COMMIT_REF_NAME}"
else
    echo "Failed to set DEPLOYER_TAG for the container"
    exit 1
fi
