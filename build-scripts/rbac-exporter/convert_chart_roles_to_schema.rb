#!/usr/bin/env ruby

# Script to read the output from `helm template` and
# convert the RBAC ServiceAccount entries into YAML
# formatted output appropriate for GKE Marketplace
#
# Consumers of this script should be aware that the
# top level entries are not indented at all when
# inserting this output into a marketplace schema.yml

require 'pathname'
require_relative 'lib/chart_objects'
require 'optparse'

script_name = File.basename($PROGRAM_NAME)

options = {}

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: #{script_name} [options]"
  opts.separator "Generate valid YAML output for insertion into a Google Marketplace schema YAML file."
  opts.separator "Arguments:"
  opts.on("-t", "--template-file TEMPLATE", "Path to a yaml output from `helm template`") do |t|
    options[:template] = t
  end
  opts.on("-m", "--mapping-file [MAPPING]", "Path to a yaml file mapping subject names to the chart variable that stores it") do |m|
    options[:mapping] = m
  end
  opts.on("-i", "--indent [INDENT]", Integer, "Specify an integer reprensenting how many spaces will be inserted before the YAML output") do |i|
    if i.negative?
      puts "Negative integers for indentation is invalid"
      exit(1)
    end
    options[:indent] = i
  end
  opts.on("-h", "--help", "Shows this message") do
    puts opt_parser
  end
end

opt_parser.parse!

if options.empty? || options[:template].nil?
  puts opt_parser
  exit(1)
end

chart_fn = options[:template]

chart_fh = Pathname.new(chart_fn)

begin
  chart_file = File.expand_path(chart_fh)

  chart_data = ChartObjects.new chart_file, options[:mapping]

  output = {}

  chart_data.kubernetes_subjects.each do |account|
    output = output.merge(account.structure)
  end

  options[:indent] = 0 unless options.key? :indent
  # remove the leading --- in the yaml because we don't want it
  puts((" " * options[:indent]) + output.to_yaml.lines[1..-1].join(" " * options[:indent]))
rescue StandardError => e
  puts e.message
  exit(1)
end
